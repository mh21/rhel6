#!/bin/bash
set -euo pipefail

yum install -y tar gcc perl

OPENSSL_VERSION="openssl-1.0.2u"

curl --silent --location "https://www.openssl.org/source/old/1.0.2/${OPENSSL_VERSION}.tar.gz" \
    | tar --extract --gzip --file - --directory /usr/src/

cd "/usr/src/${OPENSSL_VERSION}"
./config shared
make
make install INSTALL_PREFIX=/opt/

tar -zcf "${CI_PROJECT_DIR}/openssl.tgz" -C /opt/usr/local/ .

