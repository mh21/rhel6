#!/bin/bash
set -euo pipefail

yum -y install \
    tar \
    gcc \
    perl \
    libgcrypt-devel \
    libffi-devel \
    zlib-devel \
    xz-devel \
    readline-devel \
    ncurses-devel \
    bzip2-devel \
    libuuid-devel \
    sqlite-devel

tar -zxf openssl.tgz -C /usr/local/
echo "/usr/local/ssl/lib/" > /etc/ld.so.conf.d/local.conf
ldconfig 

PYTHON_VERSION="Python-3.7.0"

curl --silent --location "https://www.python.org/ftp/python/3.7.0/${PYTHON_VERSION}.tgz" \
    | tar --extract --gzip --file - --directory /usr/src/

cd /usr/src/Python-3.7.0/
./configure  --enable-optimizations
make
make install

tar -zcf "${CI_PROJECT_DIR}/python.tgz" -C /usr/local/ .

